import router from './router'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { uerserLogin } from './utils/login';
import { tokenErr } from './utils/js2androidTokenErr';
import { Toast } from 'mint-ui';
import md5 from 'js-md5';
let Base64 = require('js-base64').Base64;

NProgress.configure({ showSpinner: false }) // NProgress Configuration

router.beforeEach((to, from, next) => {
  if (to.path == "/testmath" || to.path == "/testmath1" || to.path == "/testmath2" || to.path == "/404" || to.path == "/testmath3" || to.path == "/testmath4" || to.path == "/testmath5") {
    next();
    return;
  }
  console.log("================================================beforeEach->start==========================");
  console.log(to);
  console.log(from);
  console.log(next);
  NProgress.start()
  window.localStorage.setItem('isChrome', 'false'); // 是否是在电脑Chrome上测试
  let token = to.query.token;
  let mobile = to.query.mobile;
  let password = to.query.password;
  let loginType = to.query.loginType;
  let apihost = to.query.apihost;
  let clearStorage = to.query.clearStorage;
  let strApihost;
  if (clearStorage != null && clearStorage != '' && clearStorage == 'true') {
    window.localStorage.removeItem('android_mobile');
    window.localStorage.removeItem('android_password');
    window.localStorage.removeItem('android_loginType');
    window.localStorage.removeItem('android_apihost');
    window.localStorage.removeItem('android_token');
  }
  if (mobile != null && mobile !== '') {
    window.localStorage.setItem('android_mobile', mobile);
  }
  if (password != null && password !== '') {
    window.localStorage.setItem('android_password', password);
  }
  if (loginType != null && loginType !== '') {
    window.localStorage.setItem('android_loginType', loginType);
  }
  if (apihost != null && apihost !== '') {
    strApihost = Base64.decode(apihost);
    console.log("strApihost = " + strApihost);
    window.localStorage.setItem('android_apihost', strApihost);
  }
  console.log(token);
  console.log(mobile);
  console.log(password);
  console.log(loginType);
  console.log(strApihost);

  if (token != null) {
    console.log('url传了token');
    window.localStorage.setItem('android_token', token);
    // next()必须在页面跳转之前执行，否则跳转不会生效
    delete to.query.token;
    console.log(to);
    next({ name: to.name, query: to.query });
    //NProgress.done()
  } else {
    token = window.localStorage.getItem('android_token');
    if (token == null && to.path != "/notoken") {
      console.log('没有token');
      try {
        if (window.localStorage.getItem('isChrome') === 'true') {
          console.log('isChrome=ture 去登陆');
          uerserLogin();
          next();
        } else {
          console.log('打开网页没有传token, 调用android获得token重新刷新界面');

          Toast('登录失效，重新登录');
          tokenErr();
          next({ path: '/notoken' });
        }
      } catch (e) {
        console.log('出错，重新刷新页面');
        //location.reload();
        Toast('出错了，重新登录');
        tokenErr();
        next({ path: '/notoken' });
      }
    } else {
      console.log('从localStorage获得token');
      next();
    }
  }
  console.log("================================================beforeEach->end==========================");

})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
