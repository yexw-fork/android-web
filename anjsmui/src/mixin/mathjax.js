/* eslint-disable */
// 在需要过滤的文件头部末尾分别添加：/* eslint-disable */，/* eslint-disable no-new */
/*
 * @Author: your name
 * @Date: 2020-03-23 15:20:03
 * @LastEditTime: 2020-06-10 18:09:24
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \03PcWeb\src\mixins\mathjax.js
 */


export default {
  mounted() {
    window.MathJax.Hub.Config({
      config: ["MMLorHTML.js"],
      jax: ["input/MathML", "output/HTML-CSS", "output/NativeMML"],
      extensions: ["mml2jax.js", "MathMenu.js", "MathZoom.js"]
   });

    this.$nextTick(() => {
      setTimeout(() => {
        window.UpdateMath = function (elementID) {
          MathJax.Hub.Queue(["Typeset", MathJax.Hub, elementID]);
          
       };
      }, 300)
    })

  },
  methods: {

    rewriteMathjax(domId = 'mathPage', name = '试卷', hide = false) {
      setTimeout(() => {
        this.$nextTick(() => {
          let loading = '';
          const that = this;
          if (!hide) {
            loading = this.$loading({
              lock: true,
              text: `正在解析${name}，请稍等`,
              spinner: 'el-icon-loading',
              // background: 'rgba(0, 0, 0, 0.7)',
              background: "rgba(255, 255, 255, 0.4)",
              duration: 60000
            });
          }
          // console.log(domId)
          // let txt = document.querySelectorAll('.analyseText')[0];
          // console.log(document.querySelectorAll('.analyseText')[0])
          let dom = document.getElementById(domId)
          UpdateMath(dom);
          if (!hide) {
            loading.close();
          }
        });
      }, 300)
    },
  }
}