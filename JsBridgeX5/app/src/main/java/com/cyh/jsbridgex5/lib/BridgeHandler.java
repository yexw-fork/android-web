package com.cyh.jsbridgex5.lib;

public interface BridgeHandler {
	
	void handler(String data, CallBackFunction function);

}
