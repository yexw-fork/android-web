package com.cyh.jsbridgex5;

import android.content.Context;
import android.os.Build;

import com.cyh.jsbridgex5.lib.BridgeWebView;
import com.tencent.smtt.sdk.WebSettings;

public class WebViewSet {
    public static void initSet(BridgeWebView webView, Context context) {
        webView.clearCache(true); // 清除缓存
        webView.clearHistory(); // 清除历史
        webView.clearFormData(); // 清除表单数据
        webView.setInitialScale(100);//代表不缩放。
        WebSettings ws = webView.getSettings();
        ws.setAppCacheMaxSize(1024*1024*8);
        String appCachePath = context.getCacheDir().getAbsolutePath();
        ws.setAppCachePath(appCachePath);
        ws.setAllowFileAccess(true);

        // 网页内容的宽度是否可大于WebView控件的宽度
        ws.setLoadWithOverviewMode(false);
        // 保存表单数据
        ws.setSaveFormData(true);
        // 是否应该支持使用其屏幕缩放控件和手势缩放
        ws.setSupportZoom(false);
        ws.setBuiltInZoomControls(false);
        ws.setDisplayZoomControls(false);
        // 启动应用缓存
        ws.setAppCacheEnabled(true);
        // 设置缓存模式
        ws.setCacheMode(WebSettings.LOAD_DEFAULT);
        //ws.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        //ws.setCacheMode(WebSettings.LOAD_NO_CACHE);
        // setDefaultZoom  api19被弃用
        // 设置此属性，可任意比例缩放。
        ws.setUseWideViewPort(true);
        ws.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        // 缩放比例 1
//        webView.setInitialScale(1);
        // 告诉WebView启用JavaScript执行。默认的是false。
        ws.setJavaScriptEnabled(true);
        //  页面加载好以后，再放开图片
        ws.setBlockNetworkImage(false);
        // 使用localStorage则必须打开
        ws.setDomStorageEnabled(true);
        // 排版适应屏幕
        ws.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        // WebView是否支持多个窗口。
        ws.setSupportMultipleWindows(true);

        // webview从5.0开始默认不允许混合模式,https中不能加载http资源,需要设置开启。
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ws.setMixedContentMode(WebSettings.LOAD_NORMAL);
        }
    }
}
