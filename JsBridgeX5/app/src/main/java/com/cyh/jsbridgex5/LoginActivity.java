package com.cyh.jsbridgex5;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cyh.jsbridgex5.http.ApiContainer;
import com.cyh.jsbridgex5.http.ConstantValues;
import com.cyh.jsbridgex5.http.GlobalParams;
import com.cyh.jsbridgex5.http.HttpManager;
import com.cyh.jsbridgex5.http.JsonUtils;
import com.cyh.jsbridgex5.http.LoginBean;
import com.cyh.jsbridgex5.lib.BridgeHandler;
import com.cyh.jsbridgex5.lib.BridgeWebView;
import com.cyh.jsbridgex5.lib.BridgeWebViewClient;
import com.cyh.jsbridgex5.lib.CallBackFunction;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tencent.smtt.export.external.interfaces.ConsoleMessage;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.util.ArrayList;
import java.util.List;

/**
 * 所有模块统一登录页面
 */

public class LoginActivity extends Activity{
    BridgeWebView webView;
    ListView list_view;
    ScrollView sv_testScroll;
	private String[] listArr = { "000000000000000000000000000000000000000000000000000000000000000000000",
	"1111111111111111111111111111111111111111111111111111111111111111111111111",
	"2222222222222222222222222222222222222222222222222222222222222222222",
	"3333333333333333333333333333333333333333333333333333333333",    
	"444444444444444444444444444444444444444444444444444444444",
	"555555555555555555555555555555555555555555555555555555555555555",
	"66666666666666666666666666666666666666666666666666666666666666666666666666666",
	"7777777777777777777777777777777777777777777777777777777777777777777777777777777777",
	"88888888888888888888888888888888888888888888888888888888888888888888888888888888888",
	"9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999",
    "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
	"bbbbbbbbbbbbbbbbb",
	"ccccccccccccccccc",
	"dddddddddddddddddddddddddddddd",
	"eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
	"fffffffffffffffffffffffffff",
	"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",
	"iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii",
	"jjjjjjjjjjjjjjjjjjjjjjjjjjjj",
	"kkkkkkkkkkkkkkkkkkkkkkkkkkk",
	"llllllllllllllllllllllllllllll",
	"mmmmmmmmmmmmmmmmmmmmmmmmmm",
	"nnnnnnnnnnnnnnnnnnnnnn",
	"oooooooooooooooooooooo",
	"ppppppppppppppppppppppp",
	"qqqqqqqqqqqqqqqqqqqqqqq",
	"rrrrrrrrrrrrrrrrrrrrr",
	"sssssssssssssssssssssssssssssssssssss",
	"ttttttttttttttttttttttttttt",
	"uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu",
	"vvvvvvvvvvvvvvvvvvvvvvvvvv",
	"wwwwwwwwwwwwwwwwwwwwwwww",
	"xxxxxxxxxxxxxxxxxxxxxx",
	"yyyyyyyyyyyyyyyy",
	"zzzzzzzzzzzzzzzzzzzzzzzzzzzzz"};  
    TextView tv_ScrollText;
    String srollStr = "点击某一页先判断这一页涉及到哪几个课时\n" +
            "  |--一个课时获得当前课时\n" +
            "  |--多个课时，通过课时的范围获得点击的是哪个课时（通过每题的格子获得课时的范围）\n" +
            "     |--android界面显示课时信息，通过每题的格子范围显示正在做哪一题。\n" +
            "\t    |-- 做完之后点击ok键，提交这一课时涉及的那几页一起提交。\n" +
            "\t\t\n" +
            "\t\t\n" +
            "需要返回的json字符串\n" +
            " <下面这个数组表示这一页涉及到哪几个课时>\n" +
            " [\n" +
            "\t课时的一些信息，如id，examid,bookName,subjectName,examid,status等等课时本省的信息\n" +
            "\t<下面这个数组是这一个课时包含的听力信息>\n" +
            "\t[\n" +
            "\t   听力的id，url，时长，顺序是按听力题目的顺序，等等信息\n" +
            "\t]\n" +
            "\t<下面这个数组是这一个课时涉及纸张页信息，一个课时会有好几页>\n" +
            "\t[\n" +
            "\t\t包括bookId、pageNumber、ownerNo、sectionNo、width、height、pictureUrl等等页码信息\n" +
            "\t\t<下面这个数组是这一个课时在这一页中每题的格子信息，注意！！！这些格子信息是这课时在这一页的，不是其他课时在这一页的题目>\n" +
            "\t\t[\n" +
            "\t\t\t题目的信息，题号，格子坐标（包括开始的xy坐标，和结束的xy坐标）等等信息。\n" +
            "\t\t]\n" +
            "\t]\n" +
            " ]\n" +
            " \n" +
            " \n" +
            " 课时在页面的坐标不需要后台计算，前端自己通过题目格子信息技术出来。ype\":1}\n" +
            "03-19 16:44:12.483 1263-2464/com.sunducation.wts.penbox D/OkHttp: --> END POST (86-byte body)\n" +
            "03-19 16:44:12.523 1263-2464/com.sunducation.wts.penbox D/OkHttp: <-- 200 http://test-env2-penbox.train.newpage.ink/api/penBox/assistantAnswer/gridInfo.json (39ms)\n" +
             "03-19 16:44:27.563 1263-2466/com.sunducation.wts.penbox D/OkHttp: Connection: keep-alive\n" +
            "03-19 16:44:27.563 1263-2466/com.sunducation.wts.penbox D/OkHttp: Cache-Control: no-cache, max-age=0\n" +
            "03-19 16:44:27.563 1263-2466/com.sunducation.wts.penbox D/OkHttp: {\"code\":0,\"msg\":\"成功\",\"data\":[{\"assistantName\":null,\"assistantNodeName\":null,\"bookNodeId\":3489,\"examId\":null,\"examName\":null,\"pictureList\":[{\"type\":1,\"bookId\":18,\"ownerNo\":72,\"sectionNo\":0,\"pageserial\":null,\"pageNumber\":45,\"width\":2551,\"height\":3567,\"pictureUrl\":\"http://newpage-test.oss-cn-hangzhou.aliyuncs.com/161182737335599885216.png\",\"id\":1697,\"subjectCode\":0,\"status\":0,\"gridList\":[{\"questionId\":196493,\"startX\":202.0,\"startY\":813.0,\"endX\":1260.0,\"endY\":1073.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196494,\"startX\":200.0,\"startY\":1077.0,\"endX\":1258.0,\"endY\":1253.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196495,\"startX\":204.0,\"startY\":1256.0,\"endX\":1256.0,\"endY\":1443.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196496,\"startX\":208.0,\"startY\":1447.0,\"endX\":1256.0,\"endY\":1687.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196497,\"startX\":208.0,\"startY\":1689.0,\"endX\":1256.0,\"endY\":1810.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196498,\"startX\":208.0,\"startY\":1812.0,\"endX\":1260.0,\"endY\":1933.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196499,\"startX\":202.0,\"startY\":1935.0,\"endX\":1262.0,\"endY\":3299.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196500,\"startX\":1297.0,\"startY\":571.0,\"endX\":2452.0,\"endY\":910.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196501,\"startX\":1300.0,\"startY\":917.0,\"endX\":2472.0,\"endY\":1231.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196502,\"startX\":1297.0,\"startY\":1236.0,\"endX\":2514.0,\"endY\":1355.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196503,\"startX\":1291.0,\"startY\":1357.0,\"endX\":2536.0,\"endY\":3297.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null}]}]}]}\n" +
            "03-19 16:44:27.563 1263-2466/com.sunducation.wts.penbox D/OkHttp: <-- END HTTP (2025-byte body)\n" +
            "03-19 16:44:27.563 1263-1263/com.sunducation.wts.penbox E/DBUtils: [1] insertAssistantExam: the bind value at index 2 is null\n" +
            "03-19 16:44:27.573 1263-1263/com.sunducation.wts.penbox I/UsbService: [1] getAssistantAnswerGridInfo: m1:向后台请求格子参数json:{\"gridInfoListVo\":[{\"bookId\":18,\"ownerNo\":72,\"pageNumber\":45,\"sectionNo\":0}],\"type\":1}\n" +
            "03-19 16:44:27.573 1263-2464/com.sunducation.wts.penbox D/OkHttp: --> POST http://test-env2-penbox.train.newpage.ink/api/penBox/assistantAnswer/gridInfo.json\n" +
            "03-19 16:44:27.573 1263-2464/com.sunducation.wts.penbox D/OkHttp: Content-Type: application/json; charset=utf-8\n" +
            "03-19 16:44:27.573 1263-2464/com.sunducation.wts.penbox D/OkHttp: Content-Length: 86\n" +
            "03-19 16:44:27.573 1263-2464/com.sunducation.wts.penbox D/OkHttp: X-Token: bd9a6ef49b2342729b3b404cdbc969cc\n" +
            "03-19 16:44:27.573 1263-2464/com.sunducation.wts.penbox D/OkHttp: {\"gridInfoListVo\":[{\"bookId\":18,\"ownerNo\":72,\"pageNumber\":45,\"sectionNo\":0}],\"type\":1}\n" +
            "03-19 16:44:27.573 1263-2464/com.sunducation.wts.penbox D/OkHttp: --> END POST (86-byte body)\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: <-- 200 http://test-env2-penbox.train.newpage.ink/api/penBox/assistantAnswer/gridInfo.json (35ms)\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: Server: nginx/1.16.1\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: Date: Fri, 19 Mar 2021 08:44:27 GMT\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: Content-Type: application/json;charset=UTF-8\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: Transfer-Encoding: chunked\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: Connection: keep-alive\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: Cache-Control: no-cache, max-age=0\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: {\"code\":0,\"msg\":\"成功\",\"data\":[{\"assistantName\":null,\"assistantNodeName\":null,\"bookNodeId\":3489,\"examId\":null,\"examName\":null,\"pictureList\":[{\"type\":1,\"bookId\":18,\"ownerNo\":72,\"sectionNo\":0,\"pageserial\":null,\"pageNumber\":45,\"width\":2551,\"height\":3567,\"pictureUrl\":\"http://newpage-test.oss-cn-hangzhou.aliyuncs.com/161182737335599885216.png\",\"id\":1697,\"subjectCode\":0,\"status\":0,\"gridList\":[{\"questionId\":196493,\"startX\":202.0,\"startY\":813.0,\"endX\":1260.0,\"endY\":1073.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196494,\"startX\":200.0,\"startY\":1077.0,\"endX\":1258.0,\"endY\":1253.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196495,\"startX\":204.0,\"startY\":1256.0,\"endX\":1256.0,\"endY\":1443.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196496,\"startX\":208.0,\"startY\":1447.0,\"endX\":1256.0,\"endY\":1687.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196497,\"startX\":208.0,\"startY\":1689.0,\"endX\":1256.0,\"endY\":1810.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196498,\"startX\":208.0,\"startY\":1812.0,\"endX\":1260.0,\"endY\":1933.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196499,\"startX\":202.0,\"startY\":1935.0,\"endX\":1262.0,\"endY\":3299.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196500,\"startX\":1297.0,\"startY\":571.0,\"endX\":2452.0,\"endY\":910.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196501,\"startX\":1300.0,\"startY\":917.0,\"endX\":2472.0,\"endY\":1231.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196502,\"startX\":1297.0,\"startY\":1236.0,\"endX\":2514.0,\"endY\":1355.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196503,\"startX\":1291.0,\"startY\":1357.0,\"endX\":2536.0,\"endY\":3297.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null}]}]}]}\n" +
            "03-19 16:44:27.613 1263-2464/com.sunducation.wts.penbox D/OkHttp: <-- END HTTP (2025-byte body)\n" +
            "03-19 16:44:27.613 1263-1263/com.sunducation.wts.penbox E/DBUtils: [1] insertAssistantExam: the bind value at index 2 is null\n" +
            "03-19 16:44:27.623 1263-1263/com.sunducation.wts.penbox I/UsbService: [1] getAssistantAnswerGridInfo: m1:向后台请求格子参数json:{\"gridInfoListVo\":[{\"bookId\":18,\"ownerNo\":72,\"pageNumber\":45,\"sectionNo\":0}],\"type\":1}\n" +
            "03-19 16:44:27.623 1263-2466/com.sunducation.wts.penbox D/OkHttp: --> POST http://test-env2-penbox.train.newpage.ink/api/penBox/assistantAnswer/gridInfo.json\n" +
            "03-19 16:44:27.623 1263-2466/com.sunducation.wts.penbox D/OkHttp: Content-Type: application/json; charset=utf-8\n" +
            "03-19 16:44:27.623 1263-2466/com.sunducation.wts.penbox D/OkHttp: Content-Length: 86\n" +
            "03-19 16:44:27.623 1263-2466/com.sunducation.wts.penbox D/OkHttp: X-Token: bd9a6ef49b2342729b3b404cdbc969cc\n" +
            "03-19 16:44:27.623 1263-2466/com.sunducation.wts.penbox D/OkHttp: {\"gridInfoListVo\":[{\"bookId\":18,\"ownerNo\":72,\"pageNumber\":45,\"sectionNo\":0}],\"type\":1}\n" +
            "03-19 16:44:27.623 1263-2466/com.sunducation.wts.penbox D/OkHttp: --> END POST (86-byte body)\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: <-- 200 http://test-env2-penbox.train.newpage.ink/api/penBox/assistantAnswer/gridInfo.json (52ms)\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: Server: nginx/1.16.1\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: Date: Fri, 19 Mar 2021 08:44:27 GMT\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: Content-Type: application/json;charset=UTF-8\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: Transfer-Encoding: chunked\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: Connection: keep-alive\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: Cache-Control: no-cache, max-age=0\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: {\"code\":0,\"msg\":\"成功\",\"data\":[{\"assistantName\":null,\"assistantNodeName\":null,\"bookNodeId\":3489,\"examId\":null,\"examName\":null,\"pictureList\":[{\"type\":1,\"bookId\":18,\"ownerNo\":72,\"sectionNo\":0,\"pageserial\":null,\"pageNumber\":45,\"width\":2551,\"height\":3567,\"pictureUrl\":\"http://newpage-test.oss-cn-hangzhou.aliyuncs.com/161182737335599885216.png\",\"id\":1697,\"subjectCode\":0,\"status\":0,\"gridList\":[{\"questionId\":196493,\"startX\":202.0,\"startY\":813.0,\"endX\":1260.0,\"endY\":1073.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196494,\"startX\":200.0,\"startY\":1077.0,\"endX\":1258.0,\"endY\":1253.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196495,\"startX\":204.0,\"startY\":1256.0,\"endX\":1256.0,\"endY\":1443.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196496,\"startX\":208.0,\"startY\":1447.0,\"endX\":1256.0,\"endY\":1687.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196497,\"startX\":208.0,\"startY\":1689.0,\"endX\":1256.0,\"endY\":1810.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196498,\"startX\":208.0,\"startY\":1812.0,\"endX\":1260.0,\"endY\":1933.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196499,\"startX\":202.0,\"startY\":1935.0,\"endX\":1262.0,\"endY\":3299.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196500,\"startX\":1297.0,\"startY\":571.0,\"endX\":2452.0,\"endY\":910.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196501,\"startX\":1300.0,\"startY\":917.0,\"endX\":2472.0,\"endY\":1231.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196502,\"startX\":1297.0,\"startY\":1236.0,\"endX\":2514.0,\"endY\":1355.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null},{\"questionId\":196503,\"startX\":1291.0,\"startY\":1357.0,\"endX\":2536.0,\"endY\":3297.0,\"screenId\":null,\"listeningUrl\":null,\"duration\":null,\"content\":null}]}]}]}\n" +
            "03-19 16:44:27.673 1263-2466/com.sunducation.wts.penbox D/OkHttp: <-- END HTTP (2025-byte body)\nhttp://newpage-dev.oss-cn-hangzhou.aliyuncs.com/tsdtest/A8_90_42_0E_AC_EB.png\n" +
            "注意事项：\n" +
            "1，没有对所有笔盒绑定学生，所以无法上传文件，所以全部用同一个账号登录，同时提交图片会有冲突问题，有冲突就再提交一次。（如果对所有笔盒绑定学生就没有这个问题）\n" +
            "2，笔盒需要重新升级，需要一点时间\n" +
            "3，要准备同一张纸，因为测试数据写固定的同一张图片。\n" +
            "4，学生作答时需要在纸的抬头写上自己笔的imei，方便纸找图片是方便对应上\n" +
            "5,  将笔盒纸收上来，通过imei从后台拿图片\n" +
            "6，小程序单笔，和笔盒用的纸最好是不同的，有利于区分。小程序的账号是事前绑定还是在学校在绑定？\n" +
            "\n" +
            "操作步骤：\n" +
            "1，进入主界面\n" +
            "2，按侧边的圆圈键，进入wifi界面\n" +
            "3，连接一个wifi\n" +
            "4，蓝牙笔开机，出现绿灯\n" +
            "5，同时长按左箭头和右箭头键进入测试界面，上面显示“按键B开始扫描”\n" +
            "6，按按键B,会出现一个笔的列表\n" +
            "7，按上下箭头键，绿色底条选中了你的蓝牙笔的mac地址\n" +
            "8，按勾键，进入选项界面\n" +
            "9，按F键清除缓存，如果你连接错了笔，按X键断开连接重连\n" +
            "10，按D键进入绘制界面，可以看到很多红字\n" +
            "11，拿出对应的纸，在纸的上部分，写上你笔的mac地址，\n" +
            "12，可以开始作答，可以看到红色字在动，说明正在作答\n" +
            "13，作答完成后，点击OK键，弹框，再点击ok键，等待，\n" +
            "14，如果提交失败，重新按ok键\n" +
            "15，弹出提交成功提示后，测试完成。\n" +
            "16，收集上笔盒，纸，蓝牙笔。\n" +
            "\n";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        permission();

        webView = (BridgeWebView) findViewById(R.id.webView);
        sv_testScroll = findViewById(R.id.sv_testScroll);
        tv_ScrollText = findViewById(R.id.tv_ScrollText);
        tv_ScrollText.setText(srollStr);
        list_view = findViewById(R.id.list_view);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
			LoginActivity.this, android.R.layout.simple_list_item_1, listArr);
        list_view.setAdapter(adapter);

        WebViewSet.initSet(webView, getApplicationContext());

        //如果要用这个监听一定要注意是实现BridgeWebViewClient ！！！！！！！！！！！
        webView.setWebViewClient(new BridgeWebViewClient(webView){
            @Override
            public void onPageFinished(WebView webView, String s) {
                super.onPageFinished(webView, s);
                //startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            }

            @Override
            public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                super.onReceivedSslError(webView, sslErrorHandler, sslError);
                //sslErrorHandler.proceed();
                //不加这个会报错
            }
        });

        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.i("console-js", "["+consoleMessage.messageLevel()+"] "+ consoleMessage.message() + "(" +consoleMessage.sourceId()  + ":" + consoleMessage.lineNumber()+")");
                return super.onConsoleMessage(consoleMessage);
            }
        });

        webView.registerHandler("submitFromWeb", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                Log.i("1111111111", "js-2-android =  " + data);
                function.onCallBack("android-2-js 中文 from Java");
            }
        });

        webView.registerHandler("testWindowJs", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                Log.i("1111111111", "android window测试数据  =  " + data);
                function.onCallBack("window测试数据成功，返回");
            }
        });
    }

    public void onAndroid2js(View v){
		webView.setVisibility(View.VISIBLE);
        sv_testScroll.setVisibility(View.GONE);
		list_view.setVisibility(View.GONE);
        Log.i("11111111", "onAndroid2js");
        webView.callHandler("androidToJsTest", "安卓发送过来的数据", new CallBackFunction() {

            @Override
            public void onCallBack(String data) {
                // TODO Auto-generated method stub
                Log.i("11111111", "android发送数据后，js返回的数据--" + data);
            }

        });
    }

    public void onClickWindowScroll(View v) {
        webView.setVisibility(View.VISIBLE);
        sv_testScroll.setVisibility(View.GONE);
        list_view.setVisibility(View.GONE);
        String url = "http://192.168.3.6:8080/#/testmath2";
        Log.i("11111",  url);
        webView.loadUrl(url);
    }

    public void onClickTest404(View v) {
        webView.setVisibility(View.VISIBLE);
        sv_testScroll.setVisibility(View.GONE);
		list_view.setVisibility(View.GONE);
        String url = "file:///android_asset/dist/index.html#/404";
        Log.i("11111",  url);
        webView.loadUrl(url);
    }
    public void onClickTestList(View v) {
		webView.setVisibility(View.GONE);
        sv_testScroll.setVisibility(View.GONE);
		list_view.setVisibility(View.VISIBLE);
	}
    public void onClickTestScroll(View v) {
        webView.setVisibility(View.GONE);
        sv_testScroll.setVisibility(View.VISIBLE);
		list_view.setVisibility(View.GONE);
    }

    public void onClickLogin(View v) {
        webView.setVisibility(View.VISIBLE);
        sv_testScroll.setVisibility(View.GONE);
		list_view.setVisibility(View.GONE);
		
        JsonObject params = new JsonObject();
        final String mobile = "18800000002";
        final String password = "123456";
        final String loginType = "1";


        params.addProperty("mobile", "18800000002");
        params.addProperty("password", "123456");
        params.addProperty("loginType", "1");
        String url = ConstantValues.API_HOST + ApiContainer.API_TEACHER_LOGIN;

        HttpManager.post(url, params, this, new HttpManager.OnResponse<String>() {
                    @Override
                    public void onStart() {
                        //MobclickAgentHelper.login();

                    }

                    @Override
                    public void onCompleted() {


                    }

                    @Override
                    public void onFailed(int code, String msg, String url) {

                    }

                    @Override
                    public String analyseResult(String result) {

                        return result;
                    }

                    @Override
                    public void onSuccess(String tokenRsp) {
                        Log.i("11111", tokenRsp);
                        if(JsonUtils.isGoodJson(tokenRsp)) {
                            LoginBean loginBean = JsonUtils.fromJson(tokenRsp, LoginBean.class);
                            SharedPreferences sp = getSharedPreferences("isLogin", Context.MODE_PRIVATE);
                            SharedPreferences.Editor edit = sp.edit();
                            //通过editor对象写入数据
                            edit.putString("token",loginBean.getData().getToken().trim());
                            //提交数据存入到xml文件中
                            edit.commit();
                            GlobalParams.token = loginBean.getData().getToken().trim();
                            //String url = ConstantValues.WEB_PATH + "/#/login?token=" + GlobalParams.token + "&mobile=" + mobile + "&password=" + password + "&loginType=" + loginType + "&apihost=" + Base64.encodeToString(ConstantValues.API_HOST.getBytes(), Base64.DEFAULT);
                            //String url = ConstantValues.WEB_PATH + "/#/login?mobile=" + mobile + "&password=" + password + "&loginType=" + loginType + "&apihost=" + Base64.encodeToString(ConstantValues.API_HOST.getBytes(), Base64.DEFAULT);
                            //String url = ConstantValues.WEB_PATH + "/#/testmath";
                            String url = "file:///android_asset/dist/index.html#/testmath";
                            Log.i("11111",  url);
                            webView.loadUrl(url);
                            //webview.loadUrl("javascript:window.location.reload( true )");
                            /*String url = AppConfig.BASE_WEB_URL
                                    +"questionDetail?"
                                    +"publishId=" + publishId
                                    +"&token=" + SPUtils.getInstance().getString(Constants.HEAD_XTOKEN)
                                    + "&examQuestionId=" + examQuestionId;*/
                        }
                    }
                }
        );
    }



    private static final int PERMISSION_REQUEST_CODE = 102;
    private void permission(){
        List<String> permissionLists = new ArrayList<>();
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            permissionLists.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
            permissionLists.add(Manifest.permission.READ_PHONE_STATE);
        }

        if(!permissionLists.isEmpty()){//说明肯定有拒绝的权限
            ActivityCompat.requestPermissions(this, permissionLists.toArray(new String[permissionLists.size()]), PERMISSION_REQUEST_CODE);
        }else{
            //gotoMainAct();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0){
                    for(int grantResult : grantResults){
                        if(grantResult != PackageManager.PERMISSION_GRANTED){
                            finish();
                            return;
                        }
                    }
                    //其他逻辑(这里当权限都同意的话就执行下一步逻辑)
                    //gotoMainAct();
                }
                break;
            default:
                break;
        }
    }
}
