package com.cyh.jsbridgex5.http;

public class LoginBean {
    private Integer code;
    private String msg;
    private LoginBeanData data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public LoginBeanData getData() {
        return data;
    }

    public void setData(LoginBeanData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LoginBean{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
