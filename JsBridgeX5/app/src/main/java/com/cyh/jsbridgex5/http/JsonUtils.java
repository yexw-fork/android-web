package com.cyh.jsbridgex5.http;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

/**
 * Created by Q on 2015/12/14 13:26
 */
public final class JsonUtils {

    public final static String JSON_SUFFIX = ".json";

    private final static Gson mGson = new Gson();

    private JsonUtils() {
    }

    public static boolean isBadJson(String json) {
        return !isGoodJson(json);
    }

    public static boolean isGoodJson(String json) {
        if (StringUtil.isBlank(json)) {
            return false;
        }
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static <T> T fromJson(String json, Class<T> cls) {
        return mGson.fromJson(json, cls);
    }

    public static <T> T fromJson(String json, Type typeOfT) {
        return mGson.fromJson(json, typeOfT);
    }

    public static <T> T fromJson(JsonElement json, Class<T> cls) {
        return mGson.fromJson(json, cls);
    }

    public static <T> T fromJson(JsonElement json, Type typeOfT) {
        return mGson.fromJson(json, typeOfT);
    }

    public static String toJson(JsonElement element) {
        return mGson.toJson(element);
    }

    public static String toJson(Object obj) {
        return mGson.toJson(obj);
    }

    public static String toJson(Object obj, Type typeOfObj) {
        return mGson.toJson(obj, typeOfObj);
    }

    public static JsonElement toJsonTree(Object obj) {
        return mGson.toJsonTree(obj);
    }

    public static JsonElement toJsonTree(Object obj, Type typeOfObj) {
        return mGson.toJsonTree(obj, typeOfObj);
    }
}
